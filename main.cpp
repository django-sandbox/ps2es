#include <iostream>
#include "PostgresHandler.h"
#include "Site.h"
#include "ElasticHandler.h"
#include <algorithm>
#include <chrono>

int main() {
    // object initialisation
    auto *connect_ps_site = new PostgresHandler<Site>();
    auto target_table = std::string("site");

    connect_ps_site->get_all_for_table(target_table);

    // ingest data into ES
    auto sites = connect_ps_site->get_items();
    auto *connect_es_site = new ElasticHandler<Site>();

    auto start = std::chrono::high_resolution_clock::now();
    connect_es_site->put_document_bulk(sites);
    auto stop = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count();
    std::cout << "Ingested " << sites.size() << " sites into elasticsearch in " << (duration / 1'000'000.0) << " seconds" << std::endl;


    delete connect_es_site;
    delete connect_ps_site;

    auto *connect_ps_device = new PostgresHandler<Device>();
    target_table = "device_details";

    connect_ps_device->get_all_for_table(target_table);

    // ingest data into ES
    auto devices = connect_ps_device->get_items();
    auto *connect_es_device = new ElasticHandler<Device>();

    start = std::chrono::high_resolution_clock::now();
    connect_es_device->put_document_bulk(devices);
    stop = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count();
    std::cout << "Ingested " << devices.size() << " devices into elasticsearch in " << (duration / 1'000'000.0) << " seconds" << std::endl;

    delete connect_ps_device;

    curl_global_cleanup();
    return 0;
}