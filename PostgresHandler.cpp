//
// Created by django on 23/9/19.
//

#include "PostgresHandler.h"
#include <chrono>

template <typename T>
void PostgresHandler<T>::get_all_for_table(std::string &table_name) {
    pqxx::work txn(C);

    auto sizeResult = txn.exec1("SELECT COUNT(*) FROM " + table_name + ";");
    int size = sizeResult.at(0).as<int>(0);
    std::cout << "Detected " << size << " rows in table: " << table_name << std::endl;

    cleanup();
    items.clear();
    // ensure we have enough space
    items.reserve(size);


    auto start = std::chrono::high_resolution_clock::now();
    auto result = txn.exec("SELECT * FROM " + table_name + ";");
    for(const pqxx::row &r : result) {
        // convert the row into a site
        auto *item = new T(r);
        items.push_back(item);
    }

    auto stop = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start).count();
    std::cout << "Ingested " << size << " " << table_name << "s in " << (duration / 1'000'000.0) << " seconds" << std::endl;
}
