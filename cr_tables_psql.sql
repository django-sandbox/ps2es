drop table if exists access_area;
drop table if exists antenna;
drop table if exists antenna_pattern;
drop table if exists antenna_polarity;
drop table if exists applic_text_block;
drop table if exists auth_spectrum_area;
drop table if exists auth_spectrum_freq;
drop table if exists bsl;
drop table if exists bsl_area;
drop table if exists class_of_station;
drop table if exists client;
drop table if exists client_type;
drop table if exists device_details;
drop table if exists fee_status;
drop table if exists industry_cat;
drop table if exists licence;
drop table if exists licence_service;
drop table if exists licence_status;
drop table if exists licence_subservice;
drop table if exists licensing_area;
drop table if exists nature_of_service;
drop table if exists reports_text_block;
drop table if exists satellite;
drop table if exists site;


create table access_area(
 AREA_ID		numeric(10),
 AREA_CODE              varchar(256),
 AREA_NAME              varchar(256),
 AREA_CATEGORY          numeric);

create table antenna(
 ANTENNA_ID		varchar(31),
 GAIN                   numeric,
 FRONT_TO_BACK          numeric,
 H_BEAMWIDTH            numeric,
 V_BEAMWIDTH            numeric,
 BAND_MIN_FREQ          numeric,
 BAND_MIN_FREQ_UNIT     varchar(3),
 BAND_MAX_FREQ          numeric,
 BAND_MAX_FREQ_UNIT     varchar(3),
 ANTENNA_SIZE           numeric,
 ANTENNA_TYPE           varchar(240),
 MODEL                  varchar(80),
 MANUFACTURER           varchar(255));

create table antenna_pattern(
 ANTENNA_ID		varchar(31),
 AZ_TYPE                varchar(15),
 ANGLE_REF              numeric,
 ANGLE                  numeric,
 ATTENUATION            numeric);

create table antenna_polarity(
 POLARISATION_CODE	varchar(3),
 POLARISATION_TEXT      varchar(50));

create table applic_text_block(
 APTB_ID		numeric,
 APTB_TABLE_PREFIX	varchar(30),
 APTB_TABLE_ID          numeric(10),
 LICENCE_NO             varchar(63),
 APTB_DESCRIPTION       varchar(255),
 APTB_CATEGORY          varchar(255),
 APTB_TEXT              varchar(4000),
 APTB_ITEM              varchar(15));

create table auth_spectrum_area(
 LICENCE_NO  		varchar(63),
 AREA_CODE              varchar(256),
 AREA_NAME              varchar(256),
 AREA_DESCRIPTION       text);

create table auth_spectrum_freq(
 LICENCE_NO		varchar(63),
 AREA_CODE              varchar(256),
 AREA_NAME              varchar(256),
 LW_FREQUENCY_START     numeric,
 LW_FREQUENCY_END       numeric,
 UP_FREQUENCY_START     numeric,
 UP_FREQUENCY_END       numeric);

create table bsl(
 BSL_NO                 varchar(31),
 MEDIUM_CATEGORY        varchar(4000),
 REGION_CATEGORY        varchar(4000),
 COMMUNITY_INTEREST     varchar(4000),
 BSL_STATE              varchar(4000),
 DATE_COMMENCED         DATE,
 ON_AIR_ID              varchar(511),
 CALL_SIGN              varchar(255),
 IBL_TARGET_AREA        varchar(511),
 AREA_CODE              varchar(256),
 REFERENCE              varchar(63)
);

create table bsl_area(
 AREA_CODE		varchar(256),
 AREA_NAME		varchar(256)
);

create table class_of_station(
 CODE			varchar(31),
 DESCRIPTION            varchar(511));

create table client(
 CLIENT_NO		numeric,
 LICENCEE               varchar(201),
 TRADING_NAME           varchar(100),
 ACN                    varchar(100),
 ABN                    varchar(14),
 POSTAL_STREET          varchar(600),
 POSTAL_SUBURB          varchar(480),
 POSTAL_STATE           varchar(36),
 POSTAL_POSTCODE        varchar(72),
 CAT_ID                 numeric,
 CLIENT_TYPE_ID         numeric,
 FEE_STATUS_ID          numeric);

create table client_type(
 TYPE_ID		numeric,
 NAME                   varchar(240));

create table device_details(
 SDD_ID   				numeric(10),
 LICENCE_NO                             varchar(63),
 DEVICE_REGISTRATION_IDENTIFIER         varchar(63),
 FORMER_DEVICE_IDENTIFIER               varchar(63),
 AUTHORISATION_DATE                     date,
 CERTIFICATION_METHOD                   varchar(255),
 GROUP_FLAG                             varchar(255),
 SITE_RADIUS                            numeric,
 FREQUENCY                              numeric,
 BANDWIDTH                              numeric,
 CARRIER_FREQ                           numeric,
 EMISSION                               varchar(63),
 DEVICE_TYPE                            varchar(1),
 TRANSMITTER_POWER                      numeric,
 TRANSMITTER_POWER_UNIT                 varchar(31),
 SITE_ID                                varchar(31),
 ANTENNA_ID                             varchar(31),
 POLARISATION                           varchar(3),
 AZIMUTH                                numeric,
 HEIGHT                                 numeric,
 TILT                                   numeric,
 FEEDER_LOSS                            numeric,
 LEVEL_OF_PROTECTION                    numeric,
 EIRP                                   numeric,
 EIRP_UNIT                              varchar(31),
 SV_ID                                  numeric(10),
 SS_ID                                  numeric(10),
 EFL_ID                                 varchar(40),
 EFL_FREQ_IDENT                         varchar(31),
 EFL_SYSTEM                             varchar(63),
 LEQD_MODE                              varchar(1),
 RECEIVER_THRESHOLD                     numeric,
 AREA_AREA_ID                           numeric(10),
 CALL_SIGN                              varchar(255),
 AREA_DESCRIPTION                       varchar(9),
 AP_ID                                  numeric(10),
 CLASS_OF_STATION_CODE                  varchar(31),
 SUPPLIMENTAL_FLAG                      varchar(199),
 EQ_FREQ_RANGE_MIN                      numeric,
 EQ_FREQ_RANGE_MAX                      numeric,
 NATURE_OF_SERVICE_ID                   varchar(3),
 HOURS_OF_OPERATION                     varchar(11),
 SA_ID                                  numeric(10),
 RELATED_EFL_ID                         numeric,
 EQP_ID                                 numeric(10),
 ANTENNA_MULTI_MODE                     varchar(3),
 POWER_IND                              varchar(14),
 LPON_CENTER_LONGITUDE                  numeric,
 LPON_CENTER_LATITUDE                   numeric,
 TCS_ID                                 numeric(10),
 TECH_SPEC_ID                           varchar(63),
 DROPTHROUGH_ID                         varchar(63),
 STATION_TYPE                           varchar(511),
 STATION_NAME                           varchar(63));

create table fee_status(
 FEE_STATUS_ID		numeric,
 FEE_STATUS_TEXT        varchar(100));

create table industry_cat(
 CAT_ID			numeric,
 DESCRIPTION            varchar(240),
 NAME                   varchar(120));

create table licence(
 LICENCE_NO		varchar(63),
 CLIENT_NO              numeric,
 SV_ID                  numeric(10),
 SS_ID                  numeric(10),
 LICENCE_TYPE_NAME      varchar(63),
 LICENCE_CATEGORY_NAME  varchar(95),
 DATE_ISSUED            date,
 DATE_OF_EFFECT         date,
 DATE_OF_EXPIRY         date,
 STATUS                 varchar(10),
 STATUS_TEXT            varchar(256),
 AP_ID                  numeric(10),
 AP_PRJ_IDENT           varchar(511),
 SHIP_NAME              varchar(255),
 BSL_NO                 varchar(31));

create table licence_service(
 SV_ID			numeric(10),
 SV_NAME                varchar(63));

create table licence_status(
 STATUS			varchar(10),
 STATUS_TEXT            varchar(511));

create table licence_subservice(
 SS_ID			numeric(10),
 SV_SV_ID               numeric(10),
 SS_NAME                varchar(95));

create table licensing_area(
 LICENSING_AREA_ID	varchar(31),
 DESCRIPTION            varchar(511));

create table nature_of_service(
 CODE			varchar(31),
 DESCRIPTION            varchar(511));

create table reports_text_block(
 RTB_ITEM		varchar(15),
 RTB_CATEGORY           varchar(255),
 RTB_DESCRIPTION        varchar(255),
 RTB_START_DATE         date,
 RTB_END_DATE           date,
 RTB_TEXT               varchar(4000));

create table satellite(
 SA_ID			numeric(10),
 SA_SAT_NAME            varchar(31),
 SA_SAT_LONG_NOM        numeric,
 SA_SAT_INCEXC          numeric,
 SA_SAT_GEO_POS         varchar(1),
 SA_SAT_MERIT_G_T       numeric);

create table site(
 SITE_ID		varchar(31),
 LATITUDE               numeric,
 LONGITUDE              numeric,
 NAME                   varchar(767),
 STATE                  varchar(80),
 LICENSING_AREA_ID      numeric,
 POSTCODE               varchar(18),
 SITE_PRECISION         varchar(31),
 ELEVATION              numeric,
 HCIS_L2		varchar(31));

COPY site FROM '/spectra/site.csv' DELIMITER ',' CSV HEADER;
COPY device_details FROM '/spectra/device_details.csv' DELIMITER ',' CSV HEADER;
