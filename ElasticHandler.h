//
// Created by django on 24/9/19.
//

#ifndef PSES_ELASTICHANDLER_H
#define PSES_ELASTICHANDLER_H

#include <curl/curl.h>
#include <string>
#include <iostream>
#include "Site.h"
#include "Device.h"
#include <vector>

using std::string;

template <typename T>
class ElasticHandler {
public:
//    explicit ElasticHandler(string &target_url): es_url(target_url) {
//        curl = curl_easy_init();
//    }
    ElasticHandler() {
        curl = curl_easy_init();
        es_url = "http://localhost:9200";
        headers = curl_slist_append(headers, "Accept: application/json");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "charsets: utf-8");
    }
    ~ElasticHandler() {
        curl_slist_free_all(headers);
        curl_easy_cleanup(curl);
    }
    int put_document_bulk(std::vector<T*>& items);
    int put_document(T &item);
private:
    CURL *curl;
    string es_url;
    struct curl_slist *headers = nullptr;

    /**
     * Get the url used to put a document
     * @param id the id of the document
     * @return the url to use for PUT-ing documents
     */
     [[nodiscard]]
    string put_document_url(const string &index, const string &id) const {
        return es_url + "/" + index + "/_doc/" + id;
    }
    [[nodiscard]]
    string put_bulk_url(const string &index) const {
         return es_url + "/" + index + "/_bulk";
     }
};

template class ElasticHandler<Site>;
template class ElasticHandler<Device>;


#endif //PSES_ELASTICHANDLER_H
