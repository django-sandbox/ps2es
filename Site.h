//
// Created by django on 23/9/19.
//

#ifndef PSES_SITE_H
#define PSES_SITE_H

#include <string>
#include <ostream>
#include <nlohmann/json.hpp>
#include <pqxx/pqxx>

using std::string;
using json = nlohmann::json;

class Site {
public:
    explicit Site(const pqxx::row &row):
            siteId(row.at(0).c_str()),
            latitude(row.at(1).as<double>()),
            longitude(row.at(2).as<double>()),
            name(row.at(3).c_str()),
            state(row.at(4).c_str()),
            licensingAreaId(row.at(5).as<int>(-1)),
            postcode(row.at(6).c_str()),
            sitePrecision(row.at(7).c_str()),
            elevation(row.at(8).as<int>(-1)),
            hcisL2(row.at(9).c_str())
    {}
    json asJson() {
        return json{
            {"siteId", siteId},
            {"name", name},
            {"state", state},
            {"postcode", postcode},
            {"sitePrecision", sitePrecision},
            {"hcisL2", hcisL2},
            {"latitude", latitude},
            {"longitude", longitude},
            {"licensingAreaId", licensingAreaId},
            {"elevation", elevation}
        };
    }
    string getId() {
        return siteId;
    }
    static const string getName() {
        return string("sites");
    }
    string name = "", state = "", postcode = "", sitePrecision = "", hcisL2 = "", siteId = "";
    double latitude{}, longitude{};
    int licensingAreaId{}, elevation{};
};


#endif //PSES_SITE_H
