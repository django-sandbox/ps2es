//
// Created by django on 23/9/19.
//

#ifndef PSES_DEVICE_H
#define PSES_DEVICE_H

#include <string>
#include <ostream>
#include <nlohmann/json.hpp>
#include <pqxx/pqxx>

using std::string;
using json = nlohmann::json;

class Device {
public:
    explicit Device(const pqxx::row &row):
            siteId(row.at(15).c_str()),
            sddId(row.at(0).as<int>(-1)),
            height(row.at(19).c_str()),
            frequency(row.at(8).as<long>(-1)),
            bandwidth(row.at(9).c_str()),
            transmitterPower(row.at(13).c_str()),
            polarisation(row.at(17).c_str())
    {}
    json asJson() {
        return json{
                {"siteId", siteId},
                {"sddId", sddId},
                {"height", height},
                {"frequency", frequency},
                {"bandwidth", bandwidth},
                {"transmitterPower", transmitterPower},
                {"polarisation", polarisation}
        };
    }
    const string getId() {
        return std::to_string(sddId);
    }
    static const string getName() {
        return string("device_details");
    }
    // this is missing most of its fields
    int sddId;
    long frequency;
    string siteId, polarisation, height, transmitterPower, bandwidth;
};

#endif //PSES_DEVICE_H
