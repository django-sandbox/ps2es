//
// Created by django on 24/9/19.
//

#include "ElasticHandler.h"

size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp)
{
    return size * nmemb;
}

template <typename T>
int ElasticHandler<T>::put_document(T &item) {
    if(item.getId() == "") {
        std::cerr << "Got an item with an empty id: " << item.getName() << std::endl;
        return 0;
    }
    if(curl) {
//        std::cout << "Got libcurl handler" << std::endl;
        // customise the request
        auto url = put_document_url(item.getName(), item.getId());
//        std::cout << "Targeting url: " << url << std::endl;
        string payload = item.asJson();
//        std::cout << "Sending payload: " << payload.c_str() << std::endl;
//        std::cout << "Using headers: " << headers->data << std::endl;

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data); // disable messages
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.c_str());

        // perform the request
//        std::cout << "Making index request for item with id: " << item.getId() << std::endl;
        auto res = curl_easy_perform(curl);
        if(res != CURLE_OK) {
            std::cerr << "HTTP request failed: " << curl_easy_strerror(res) << std::endl;
            return 0;
        }
//        std::cout << "\nHTTP response: " << res << std::endl;
        return 1;
    }
    std::cerr << "Failed to get a curl handler" << std::endl;
    return 0;
}

template<typename T>
int ElasticHandler<T>::put_document_bulk(std::vector<T *> &items) {

    string payload;
    for(auto i : items) {
        payload += "{\"index\": {} }\n";
        payload += (i->asJson().dump() + "\n");
    }
    if(curl) {
        std::cout << "Got libcurl handler" << std::endl;
        // customise the request
        string url = put_bulk_url(T::getName());
        std::cout << "Targeting url: " << url << std::endl;
        std::cout << "Sending payload of size: " << payload.size() << std::endl;

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data); // disable messages
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.c_str());
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        // perform the request
//        std::cout << "Making index request for item with id: " << item.getId() << std::endl;
        auto res = curl_easy_perform(curl);
        if(res != CURLE_OK) {
            std::cerr << "HTTP request failed: " << curl_easy_strerror(res) << std::endl;
            return 0;
        }
//        std::cout << "\nHTTP response: " << res << std::endl;
        return 1;
    }
    std::cerr << "Failed to get a curl handler" << std::endl;
    return 0;
}
