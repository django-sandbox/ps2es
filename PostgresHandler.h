//
// Created by django on 23/9/19.
//

#ifndef PSES_POSTGRESHANDLER_H
#define PSES_POSTGRESHANDLER_H


#include <pqxx/pqxx>
#include <iostream>
#include "Site.h"
#include "Device.h"

template <typename T>
class PostgresHandler {
public:
    PostgresHandler() {
        std::cout << "Opening new connection to PSQL" << std::endl;
        init();
    }
    ~PostgresHandler() {
        std::cout << "Closing connection to PSQL" << std::endl;
        cleanup();
    }
    void get_all_for_table(std::string &table_name);
    std::vector<T*>& get_items() {
        return items;
    }
private:
    pqxx::connection C{"postgresql://postgres:password@localhost:5432/postgres"};
    /**
     * Open a connection to Postgres
     */
    void init() {
        try {
            std::cout << "Connected to " << C.dbname() << std::endl;
        }
        catch (const std::exception &e) {
            std::cerr << "Caught exception in psql: " << e.what() << std::endl;
        }
    }
    void cleanup() {
        std::cout << "Cleaning items" << std::endl;
        std::for_each(items.begin(), items.end(), [](T* s) { delete s; });
    }
    std::vector<T*> items;
};

template class PostgresHandler<Site>;
template class PostgresHandler<Device>;


#endif //PSES_POSTGRESHANDLER_H
