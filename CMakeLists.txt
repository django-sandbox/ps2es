cmake_minimum_required(VERSION 3.14)
project(pses)

set(CMAKE_CXX_STANDARD 17)

if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup()
else()
    message(WARNING "The file conanbuildinfo.cmake doesn't exist, you have to run conan install first")
endif()

add_executable(pses main.cpp PostgresHandler.cpp PostgresHandler.h Site.h Device.h ElasticHandler.cpp ElasticHandler.h)
target_link_libraries(pses ${CONAN_LIBS})